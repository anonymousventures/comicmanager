package com.ntomita.comicmanager;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.futuremind.recyclerviewfastscroll.FastScroller;
import com.ntomita.comicmanager.db.BookEntry;
import com.ntomita.comicmanager.db.CatalogDbHelper;

import java.util.ArrayList;


public class AllFragment extends Fragment {
    final String TAG = this.getClass().getSimpleName();
    RecyclerView recyclerView;
    RecyclerView.Adapter adapter;
    RecyclerView.LayoutManager layoutManager;
    FloatingActionButton mSearchButton;
    public static Context mContext;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View v = inflater.inflate(R.layout.fragment_all, container, false);

        mContext = getContext();
        recyclerView = (RecyclerView) v.findViewById(R.id.recyclerview_all);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);

        // READ DB
        CatalogDbHelper catalogDbHelper = new CatalogDbHelper(getActivity());
        ArrayList<BookEntry> mDataset = catalogDbHelper.queryEntriesWithoutNestedFile();

        adapter = new RecyclerViewAdapter(getContext(), mDataset);
        recyclerView.setAdapter(adapter);

        FastScroller fastscroller = (FastScroller) v.findViewById(R.id.fastscroll_all);
        fastscroller.setRecyclerView(recyclerView);

        mSearchButton = (FloatingActionButton) v.findViewById(R.id.search_icon);
        Drawable icon = DrawableCompat.wrap(mContext.getResources().getDrawable(R.drawable.ic_search_36dp));
        DrawableCompat.setTint(icon.mutate(), Color.WHITE);
        mSearchButton.setImageDrawable(icon);


        return v;
    }

}