package com.ntomita.comicmanager;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.futuremind.recyclerviewfastscroll.FastScroller;
import com.ntomita.comicmanager.db.BookEntry;
import com.ntomita.comicmanager.db.FavDbHelper;

import java.util.ArrayList;

/**
 * Created by Naofumi on 2/23/16.
 */
public class FavFragment extends Fragment {
    final String TAG = this.getClass().getSimpleName();
    RecyclerView recyclerView;
    RecyclerView.Adapter adapter;
    RecyclerView.LayoutManager layoutManager;
    ArrayList<BookEntry> mDataset;
    FavDbHelper favDbHelper;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View v = inflater.inflate(R.layout.fragment_fav, container, false);

        recyclerView = (RecyclerView) v.findViewById(R.id.recyclerview_fav);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);

        // READ DB
        favDbHelper = new FavDbHelper(getActivity());
        mDataset = favDbHelper.queryFavs();

        adapter = new RecyclerViewAdapter(getContext(), mDataset);
        recyclerView.setAdapter(adapter);

        FastScroller fastscroller = (FastScroller) v.findViewById(R.id.fastscroll_fav);
        fastscroller.setRecyclerView(recyclerView);

        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        updateFavView();
    }

    public void updateFavView(){
        if (mDataset != null){
            mDataset.clear();
            mDataset.addAll(favDbHelper.queryFavs());
            adapter.notifyDataSetChanged();
        }
    }
}
