package com.ntomita.comicmanager;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.PowerManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;

import javax.net.ssl.HttpsURLConnection;

public class ReaderActivity extends AppCompatActivity {
    final String TAG = this.getClass().getSimpleName();
    Context mContext;

    ImageView mPageImageView;
    String fullPath;
    ZipFile mZip;
    ZipInputStream mZipInStream;

    ArrayList<String> mPageName = new ArrayList<>();
    private GestureDetector mGestureDetector;
    int pageNumber = 0;

    boolean needDownloadNextBook = false;
    boolean completedDownloadNextBook = false;
    String nextPath;
    DownloadFileTask task;

    /*
    *   LIFE CYCLE
    * */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reader);

        if (savedInstanceState != null){
            pageNumber = savedInstanceState.getInt("pageNumber");
            needDownloadNextBook = savedInstanceState.getBoolean("needDownloadNextBook");
            completedDownloadNextBook = savedInstanceState.getBoolean("completedDownloadNextBook");
        }

        mContext = this;

        GestureListener mGestureListener = new GestureListener();
        mGestureDetector = new GestureDetector(this, mGestureListener);

        mPageImageView = (ImageView) findViewById(R.id.page_image_view);
        fullPath = getIntent().getStringExtra("FULLPATH");
        try{
            mZip = new ZipFile(fullPath);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (mZip == null){
            Toast.makeText(mContext, "File is broken. Please delete and download it again", Toast.LENGTH_LONG).show();
        }

        // GET ALL PAGES TITLE (FILE NAME)
        Enumeration<? extends ZipEntry> entries = mZip.entries();
        while(entries.hasMoreElements()){
            ZipEntry entry = entries.nextElement();
            if (isImageFile(entry)){
                mPageName.add(entry.getName());
            }
        }

        displayPageAt(pageNumber);

        Intent i = getIntent();
        nextPath = i.getStringExtra("NEXT_PATH");
        String nextURL = i.getStringExtra("NEXT_URL");
        if (nextPath != null && nextURL != null){
            needDownloadNextBook = true;
            downloadBook(this, nextPath, nextURL);
        }
    }



    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("pageNumber", pageNumber);
        outState.putBoolean("needDownloadNextBook", needDownloadNextBook);
        outState.putBoolean("completedDownloadNextBook", completedDownloadNextBook);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (needDownloadNextBook && ! completedDownloadNextBook){
            task.cancel(true);
            final File tempFile = new File(Environment.getExternalStorageDirectory().getAbsolutePath()+ "/" + mContext.getApplicationInfo().loadLabel(mContext.getPackageManager()) + "/Books/"+nextPath);
            if (tempFile.exists()){
                tempFile.delete();
            }
        }
    }

    /*
    *   END: LIFE CYCLE
    * */


    /*
    *   SUPPORT FUNCTIONS
    * */
    private void displayPageAt(int page){
        Bitmap mBitmap = null;
        InputStream in = null;
        ZipEntry entry = mZip.getEntry(mPageName.get(page));
        try{
            in = mZip.getInputStream(entry);
            mBitmap = BitmapFactory.decodeStream(in);
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            if (in != null){
                try{
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }
        if (mBitmap != null){
            mPageImageView.setImageBitmap(mBitmap);
        }
    }

    private boolean isImageFile(ZipEntry entry){
        String name = entry.getName();
        if (name.endsWith(".jpg") || name.endsWith(".JPG") || name.endsWith(".jpeg") || name.endsWith(".JPEG") || name.endsWith(".png") || name.endsWith(".PNG") || name.endsWith(".gif") || name.endsWith(".GIF") || name.endsWith(".tiff") || name.endsWith(".TIFF")){
            return true;
        }else{
            return false;
        }

    }


    private void downloadBook(Context context, String bookPath, String bookURL){
        String[] params = new String[2];
        params[0] = bookURL.replaceFirst("redir", "download");
        Log.d("URL:", params[0]);
        params[1] = context.getApplicationInfo().loadLabel(context.getPackageManager()) + "/Books/" + bookPath;


        task = new DownloadFileTask(context);
        task.execute(params);
    }

    /*
    *   TOUCH EVENT
    * */

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        mGestureDetector.onTouchEvent(event);
        return super.onTouchEvent(event);
    }

    private final class GestureListener extends GestureDetector.SimpleOnGestureListener {

        private static final int SWIPE_THRESHOLD = 100;
        private static final int SWIPE_VELOCITY_THRESHOLD = 120;

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
                               float velocityY) {
            boolean result = false;
            try {
                float diffY = e2.getY() - e1.getY();
                float diffX = e2.getX() - e1.getX();
                if (Math.abs(diffX) > Math.abs(diffY)) {
                    if (Math.abs(diffX) > SWIPE_THRESHOLD
                            && Math.abs(velocityX) > SWIPE_VELOCITY_THRESHOLD) {
                        if (diffX > 0) {
                            onSwipeLeft();
                        } else {
                            onSwipeRight();
                        }
                    }
                    result = true;
                }
                result = true;
            } catch (Exception exception) {
                exception.printStackTrace();
            }

            return result;
        }

    }

    private void onSwipeLeft(){
        // prev
        if (pageNumber != 0){
            Bitmap mBitmap = null;
            InputStream in = null;
            ZipEntry entry = mZip.getEntry(mPageName.get(--pageNumber));
            try{
                in = mZip.getInputStream(entry);
                mBitmap = BitmapFactory.decodeStream(in);
            } catch (IOException e) {
                e.printStackTrace();
            }finally {
                if (in != null){
                    try{
                        in.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            }
            if (mBitmap != null){
                mPageImageView.setImageBitmap(mBitmap);
            }
        }


    }

    private void onSwipeRight(){
        // next
        if (pageNumber < mPageName.size() - 1){
            Bitmap mBitmap = null;
            InputStream in = null;
            ZipEntry entry = mZip.getEntry(mPageName.get(++pageNumber));
            try{
                in = mZip.getInputStream(entry);
                mBitmap = BitmapFactory.decodeStream(in);
            } catch (IOException e) {
                e.printStackTrace();
            }finally {
                if (in != null){
                    try{
                        in.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            }
            if (mBitmap != null){
                mPageImageView.setImageBitmap(mBitmap);
            }
        }
    }

    /*
   *   Asynctasks
   * */
    public class DownloadFileTask extends AsyncTask<String, Integer, String> {
        private Context context;
        private PowerManager.WakeLock mWakeLock;

        public DownloadFileTask(Context context){
            this.context = context;
        }

        @Override
        protected String doInBackground(String... params) {
            InputStream inStream=null;
            OutputStream outStream=null;
            HttpsURLConnection connection=null;

            try{
                URL url = new URL(params[0]);
                connection = (HttpsURLConnection) url.openConnection();
                connection.setInstanceFollowRedirects(true);
                //connection.setDoInput(true);
                //connection.setDoOutput(true);
                connection.connect();

                if (connection.getResponseCode() != HttpURLConnection.HTTP_OK){
                    return "Server returned HTTP " + connection.getResponseCode() + " " + connection.getResponseMessage();
                }

                int fileLength = connection.getContentLength();

                inStream = connection.getInputStream();
                File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath(), params[1]);
                outStream = new FileOutputStream(file);

                byte data[] = new byte[4096];
                long total = 0;
                int count;
                while ((count = inStream.read(data)) != -1){
                    if (isCancelled()){
                        inStream.close();
                        return null;
                    }
                    total += count;
                    outStream.write(data, 0, count);
                }

            }catch (MalformedURLException e){
                e.printStackTrace();
            }catch (IOException e){
                e.printStackTrace();
            } finally {
                try{
                    if (outStream != null){
                        outStream.close();
                    }
                    if (inStream != null){
                        inStream.close();
                    }
                }catch (IOException e){
                    e.printStackTrace();
                }
                if (connection != null){
                    connection.disconnect();
                }
            }

            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            PowerManager pm = (PowerManager) context.getSystemService(context.POWER_SERVICE);
            mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, getClass().getName());
            mWakeLock.acquire();
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(String s) {
            //super.onPostExecute(s);
            mWakeLock.release();
            if (s != null){
                Toast.makeText(context, "Download failed: " + s, Toast.LENGTH_LONG).show();
            }else{
                Toast.makeText(context, "File downloaded", Toast.LENGTH_SHORT).show();
            }
            completedDownloadNextBook = true;

        }
    }
}
