package com.ntomita.comicmanager.db;

import java.util.Date;

/**
 * Created by Naofumi on 2/4/16.
 */
public class BookEntry implements Comparable{
    private String mKey;
    private String mPath;
    private String mAuthor;
    private String mTitle;
    private String mURL;
    private Date mDate;
    private Boolean mIsFolder;
    private String mParent;

    public String getKey() {
        return mKey;
    }

    public void setKey(String mKey) {
        this.mKey = mKey;
    }

    public String getPath() {
        return mPath;
    }

    public void setPath(String mPath) {
        this.mPath = mPath;
    }

    public String getURL() {
        return mURL;
    }

    public void setURL(String mURL) {
        this.mURL = mURL;
    }

    public Date getDate() {
        return mDate;
    }

    public void setDate(Date mDate) {
        this.mDate = mDate;
    }

    public Boolean getIsFolder() {
        return mIsFolder;
    }

    public void setIsFolder(Boolean mIsFolder) {
        this.mIsFolder = mIsFolder;
    }

    public String getParent() {
        return mParent;
    }

    public void setParent(String mParent) {
        this.mParent = mParent;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String mTitle) {
        this.mTitle = mTitle;
    }

    public String getAuthor() {
        return mAuthor;
    }

    public void setAuthor(String mAuthor) {
        this.mAuthor = mAuthor;
    }

    @Override
    public int compareTo(Object another) {
        int val = this.getPath().compareTo(((BookEntry)another).getPath());
        if (val == 0){
            if (this.getIsFolder()){
                val = 1;
            }else{
                val = -1;
            }
        }
        return val;
    }
}
