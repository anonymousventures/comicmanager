package com.ntomita.backend.db;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Transaction;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Naofumi on 3/2/16.
 */
public class UserDatastore {
    private static final Logger mLogger = Logger.getLogger(UserDatastore.class.getName());
    private static final DatastoreService mDatastore = DatastoreServiceFactory.getDatastoreService();

    private static Key getKey() {
        return KeyFactory.createKey(User.USER_PARENT_ENTITY_NAME, User.USER_PARENT_KEY_NAME);
    }


    public static boolean add(User user) {
        if (getUserById(user.mUserId, null) != null) {
            mLogger.log(Level.INFO, "user exists");
            return false;
        }

        Key parentKey = getKey();

        Entity entity = new Entity(User.USER_ENTITY_NAME, user.mUserId, parentKey);
        entity.setProperty(User.FIELD_NAME_USER_ID, user.mUserId);
        entity.setProperty(User.FIELD_NAME_USER_NAME, user.mUserName);
        entity.setProperty(User.FIELD_NAME_GROUP_NAME, user.mGroupName);

        mDatastore.put(entity);

        return true;
    }

    public static boolean delete(String uid) {
        Query.Filter filter = new Query.FilterPredicate(Entity.KEY_RESERVED_PROPERTY,
                Query.FilterOperator.EQUAL,
                KeyFactory.createKey(getKey(), User.USER_ENTITY_NAME, uid));
        Query query = new Query(User.USER_ENTITY_NAME);
        query.setFilter(filter);

        PreparedQuery pq = mDatastore.prepare(query);

        Entity result = pq.asSingleEntity();
        boolean ret = false;
        if (result != null) {
            mDatastore.delete(result.getKey());
            ret = true;
        }
        return ret;
    }

    public static ArrayList<User> query() {

        ArrayList<User> resultList = new ArrayList<>();

        Query query = new Query(User.USER_ENTITY_NAME);
        query.setFilter(null);
        query.setAncestor(getKey());

        PreparedQuery pq = mDatastore.prepare(query);
        for (Entity entity : pq.asIterable()) {
            User user = getUserFromEntity(entity);
            if (user != null) {
                resultList.add(user);
            }
        }
        return resultList;
    }

    public static ArrayList<User> getUsersInGroup(String gname) {
        ArrayList<User> resultList = new ArrayList<>();
        ArrayList<User> users = query();
        for (User user : users) {
            if (user.mGroupName == gname) {
                resultList.add(user);
            }
        }
        return resultList;
    }

    public static User getUserById(String uid, Transaction txn) {
        Entity result = null;
        try {
            result = mDatastore.get(KeyFactory.createKey(getKey(),
                    User.USER_ENTITY_NAME, uid));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return getUserFromEntity(result);
    }

    private static User getUserFromEntity(Entity entity) {
        if (entity == null) {
            return null;
        }
        return new User(
                (String) entity.getProperty(User.FIELD_NAME_USER_ID),
                (String) entity.getProperty(User.FIELD_NAME_USER_NAME),
                (String) entity.getProperty(User.FIELD_NAME_GROUP_NAME)
        );

    }
}