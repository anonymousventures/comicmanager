package com.ntomita.comicmanager;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.futuremind.recyclerviewfastscroll.FastScroller;
import com.ntomita.comicmanager.db.BookEntry;
import com.ntomita.comicmanager.db.CatalogDbDiffHelper;

import java.util.ArrayList;


public class HomeFragment extends Fragment{
    final String TAG = this.getClass().getSimpleName();
    RecyclerView recyclerView;
    RecyclerView.Adapter adapter;
    RecyclerView.LayoutManager layoutManager;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View v = inflater.inflate(R.layout.fragment_home, container, false);

        recyclerView = (RecyclerView) v.findViewById(R.id.recyclerview_home);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);

        // READ DB
        CatalogDbDiffHelper catalogDbDiffHelper = new CatalogDbDiffHelper(getActivity());
        ArrayList<BookEntry> mDataset = catalogDbDiffHelper.queryEntries();

        adapter = new RecyclerViewAdapter(getContext(), mDataset);
        recyclerView.setAdapter(adapter);

        FastScroller fastscroller = (FastScroller) v.findViewById(R.id.fastscroll_home);
        fastscroller.setRecyclerView(recyclerView);


        return v;
    }
}

