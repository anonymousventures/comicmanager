package com.ntomita.backend.db;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Transaction;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Naofumi on 3/2/16.
 */

/*
*   METHODS: ADD, DELETE, GETFAVSBYUSER(), GETFAVBYID() where ID is uid+bid,
* */

public class FavDatastore {
    private static final Logger mLogger = Logger.getLogger(FavDatastore.class.getName());
    private static final DatastoreService mDatastore = DatastoreServiceFactory.getDatastoreService();

    private static Key getKey() {
        return KeyFactory.createKey(Fav.FAV_PARENT_ENTITY_NAME, Fav.FAV_PARENT_KEY_NAME);
    }


    public static boolean add(Fav fav) {
        if (getFavByIds(fav.mUserId, fav.mBookId, null) != null) {
            mLogger.log(Level.INFO, "fav exists");
            return false;
        }

        Key parentKey = getKey();

        Entity entity = new Entity(Fav.FAV_ENTITY_NAME, fav.mUserId+fav.mBookId, parentKey);
        entity.setProperty(Fav.FIELD_NAME_USER_ID, fav.mUserId);
        entity.setProperty(Fav.FIELD_NAME_BOOK_ID, fav.mBookId);
        mDatastore.put(entity);

        return true;
    }

    public static boolean delete(String uid, String bid){
        Query.Filter filter = new Query.FilterPredicate(Entity.KEY_RESERVED_PROPERTY,
                Query.FilterOperator.EQUAL,
                KeyFactory.createKey(getKey(),
                Fav.FAV_ENTITY_NAME, uid+bid));
        Query query = new Query(Fav.FAV_ENTITY_NAME);
        query.setFilter(filter);

        PreparedQuery pq = mDatastore.prepare(query);

        Entity result = pq.asSingleEntity();
        boolean ret = false;
        if (result != null){
            mDatastore.delete(result.getKey());
            ret = true;
        }
        return ret;
    }

    public static ArrayList<Fav> query(){

        ArrayList<Fav> resultList = new ArrayList<>();

        Query query = new Query(Fav.FAV_ENTITY_NAME);
        query.setFilter(null);
        query.setAncestor(getKey());

        PreparedQuery pq = mDatastore.prepare(query);
        for (Entity entity: pq.asIterable()){
            Fav fav = getFavFromEntity(entity);
            if (fav != null){
                resultList.add(fav);
            }
        }
        return resultList;
    }

    public static ArrayList<Fav> getFavsWithUser(String uid){
        ArrayList<Fav> resultList = new ArrayList<>();
        ArrayList<Fav> favs = query();
        for (Fav fav: favs){
            if (fav.mUserId == uid){
                resultList.add(fav);
            }
        }
        return resultList;
    }

    public static Fav getFavByIds(String uid, String bid, Transaction txn) {
        Entity result = null;
        try {
            result = mDatastore.get(KeyFactory.createKey(getKey(),
                    Fav.FAV_ENTITY_NAME, uid+bid));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return getFavFromEntity(result);
    }

    private static Fav getFavFromEntity(Entity entity){
        if (entity == null){
            return null;
        }
        return new Fav(
                (String) entity.getProperty(Fav.FIELD_NAME_USER_ID),
                (String) entity.getProperty(Fav.FIELD_NAME_BOOK_ID)
        );

    }
}
