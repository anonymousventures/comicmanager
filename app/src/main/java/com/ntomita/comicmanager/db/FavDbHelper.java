package com.ntomita.comicmanager.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.util.Log;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by Naofumi on 2/23/16.
 */
public class FavDbHelper extends SQLiteOpenHelper {
    Context mContext;
    public static String DATABASE_FILE_PATH = Environment.getExternalStorageDirectory().getAbsolutePath();//getAbsolutePath();
    private static String DATABASE_FILE_NAME = "fav.db";
    private static int DATABASE_VERSION =1;

    private static ArrayList<BookEntry> allEntries;

    private static final String TABLE_NAME = "ITEMS";
    private static final String COLUMN_ID = "_id";
    private static final String COLUMN_KEY = "KEY";

    public FavDbHelper(Context context){
        // context.getExternalFilesDir(null).getAbsolutePath()
        super(context, DATABASE_FILE_PATH + "/" + context.getApplicationInfo().loadLabel(context.getPackageManager()) + "/System/" + DATABASE_FILE_NAME, null, DATABASE_VERSION);
        mContext = context;
        //context.openOrCreateDatabase(DATABASE_FILE_PATH + "/" + context.getApplicationInfo().loadLabel(context.getPackageManager()) + "/System/" + DATABASE_FILE_NAME, context.MODE_PRIVATE, null);
        Log.d("FavDB: ", DATABASE_FILE_PATH);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS " + TABLE_NAME +
                "(" + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_KEY + " TEXT NOT NULL" + ")" );
        CatalogDbHelper dbHelper = new CatalogDbHelper(mContext);
        allEntries = dbHelper.queryEntries();
        dbHelper.close();
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public ArrayList<BookEntry> queryFavs(){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_NAME, null, null, null, null, null, null);
        ArrayList<BookEntry> favs = new ArrayList<BookEntry>();
        if (allEntries == null){
            CatalogDbHelper dbHelper = new CatalogDbHelper(mContext);
            allEntries = dbHelper.queryEntries();
            dbHelper.close();
        }

        try{
            String target;
            cursor.moveToFirst();
            while(! cursor.isAfterLast()){
                target = cursor.getString(cursor.getColumnIndex(COLUMN_KEY));
                for (BookEntry entry: allEntries){
                    if (target.equals(entry.getKey())){
                        favs.add(entry);
                    }
                }
                cursor.moveToNext();
            }
        } finally {
            cursor.close();
        }
        db.close();
        Collections.sort(favs);
        return favs;
    }

    public long addFav(String key){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_KEY, key);
        long newRowId;
        newRowId = db.insert(TABLE_NAME, null, values);
        db.close();
        return newRowId;
    }

    public void deleteFav(String key){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME, COLUMN_KEY + "=\'"+key+"\'", null);
        db.close();
    }

    public boolean isFaved(String key){
        ArrayList<BookEntry> favs = queryFavs();
        if (favs.size() == 0){
            return false;
        }
        for (BookEntry entry: favs){
            if (entry.getKey().equals(key)){
                return true;
            }
        }
        return false;

    }
}
