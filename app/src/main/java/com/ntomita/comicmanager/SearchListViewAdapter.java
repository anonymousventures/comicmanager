package com.ntomita.comicmanager;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ntomita.comicmanager.db.BookEntry;

import java.util.ArrayList;

/**
 * Created by Naofumi on 2/21/16.
 */
public class SearchListViewAdapter extends ArrayAdapter<BookEntry> {
    final String TAG = this.getClass().getSimpleName();
    ListViewHolder viewHolder;
    Context mContext;
    Drawable mFolderIcon;
    Drawable mFileIcon;

    final String FOLDER_COLOR = "#FFC107";
    final String FILE_COLOR = "#03A9F4";

    private static class ListViewHolder{
        private ImageView iconView;
        private TextView titleView;
    }

    public SearchListViewAdapter(Context context, int resource, ArrayList<BookEntry> objects) {
        super(context, resource, objects);
        mContext = context;
        mFolderIcon = DrawableCompat.wrap(mContext.getResources().getDrawable(R.drawable.ic_folder_24dp));
        DrawableCompat.setTint(mFolderIcon.mutate(), Color.parseColor(FOLDER_COLOR));
        mFileIcon = DrawableCompat.wrap(mContext.getResources().getDrawable(R.drawable.ic_insert_drive_file_24dp));
        DrawableCompat.setTint(mFileIcon.mutate(), Color.parseColor(FILE_COLOR));
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        if (convertView == null){
            convertView = LayoutInflater.from(mContext).inflate(R.layout.list_item, parent, false);
            viewHolder = new ListViewHolder();
            viewHolder.iconView = (ImageView) convertView.findViewById(R.id.item_icon_list_view);
            viewHolder.titleView = (TextView) convertView.findViewById(R.id.item_title_list_view);
            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ListViewHolder) convertView.getTag();
        }

        final BookEntry item = getItem(position);
        // SET ICON DEPENDS ON ITEM TYPE(FILE/FOLDER)
        if (item != null){
            if (item.getIsFolder()){
                viewHolder.iconView.setImageDrawable(mFolderIcon);

            }else{
                viewHolder.iconView.setImageDrawable(mFileIcon);
            }
            viewHolder.titleView.setText(item.getAuthor() + "\n" + item.getTitle());
        }

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("ItemViewHolder", "onClick");
                Intent i = new Intent(mContext, DetailActivity.class);
                i.putExtra("KEY", item.getKey());
                i.putExtra("PATH", item.getPath());
                i.putExtra("AUTHOR", item.getAuthor());
                i.putExtra("TITLE", item.getTitle());
                i.putExtra("URL", item.getURL());
                i.putExtra("ISFOLDER", item.getIsFolder());
                i.putExtra("PARENT", item.getParent());
                mContext.startActivity(i);
            }
        });

        return convertView;
    }
}
