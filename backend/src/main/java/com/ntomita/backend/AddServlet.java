package com.ntomita.backend;

import com.ntomita.backend.db.Fav;
import com.ntomita.backend.db.FavDatastore;
import com.ntomita.backend.db.User;
import com.ntomita.backend.db.UserDatastore;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by Naofumi on 3/1/16.
 */
public class AddServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //super.doGet(req, resp);
        String user_id = req.getParameter("user_id");
        String user_name = req.getParameter("user_name");
        String book_id = req.getParameter("book_id");
        String group_name = req.getParameter("group_name");

        if (user_id == null || user_id.equals("") || book_id == null || book_id.equals("")){
            return;
        }

        User user = new User(user_id, user_name, group_name);
        Fav fav = new Fav(user_id, book_id);
        boolean retu = UserDatastore.add(user);
        boolean retf = FavDatastore.add(fav);
        if (retu){
            // user info stored
        }else{
            // existing user
        }
        if (retf){
            // fav stored
        }else{
            // this fav already exists
        }




    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //super.doPost(req, resp);
        doGet(req, resp);
    }
}
