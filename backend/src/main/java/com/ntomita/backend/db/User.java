package com.ntomita.backend.db;

/**
 * Created by Naofumi on 3/1/16.
 */
public class User {
    public static final String USER_PARENT_ENTITY_NAME = "UserParent";
    public static final String USER_PARENT_KEY_NAME = "UserParent";

    public static final String USER_ENTITY_NAME = "User";
    public static final String FIELD_NAME_USER_ID = "UserId";
    public static final String FIELD_NAME_USER_NAME = "UserName";
    public static final String FIELD_NAME_GROUP_NAME = "GroupName";

    public String mUserId;
    public String mUserName;
    public String mGroupName;

    public User(String _uid, String _uname, String _gname){
        mUserId = _uid;
        mUserName = _uname;
        mGroupName = _gname;
    }

}
