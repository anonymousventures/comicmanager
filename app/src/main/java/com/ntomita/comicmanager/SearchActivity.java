package com.ntomita.comicmanager;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ListView;
import android.widget.SearchView;

import com.ntomita.comicmanager.db.BookEntry;
import com.ntomita.comicmanager.db.CatalogDbHelper;

import java.util.ArrayList;

/**
 * Created by Naofumi on 2/23/16.
 */
public class SearchActivity extends AppCompatActivity implements SearchView.OnQueryTextListener, SearchView.OnCloseListener{

    final String TAG = this.getClass().getSimpleName();
    private SearchView mSearchView;
    private ListView mListView;
    private CatalogDbHelper mDbHelper;
    private SearchListViewAdapter mAdapter;
    private ArrayList<BookEntry> mEntries;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        mSearchView = (SearchView) findViewById(R.id.search_view);
        mSearchView.setIconified(false);
        mSearchView.setOnQueryTextListener(this);
        mSearchView.setOnCloseListener(this);
        mSearchView.setSubmitButtonEnabled(true);
        mSearchView.setBackgroundColor(Color.parseColor("#FF4081"));
        mSearchView.setQueryHint("Search Library");

        mEntries = new ArrayList<>();
        mAdapter = new SearchListViewAdapter(SearchActivity.this, R.layout.list_item, mEntries);
        mListView = (ListView) findViewById(R.id.item_search_list_view);
        mListView.setAdapter(mAdapter);

        mDbHelper = new CatalogDbHelper(this);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public boolean onClose() {
        mListView.setAdapter(mAdapter);
        return false;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        updateList(query);
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if (!newText.isEmpty()){
            updateList(newText);
        }else{
            mListView.setAdapter(mAdapter);
        }
        return false;
    }

    private void updateList(String query){
        mEntries.clear();
        mEntries.addAll(mDbHelper.queryEntriesWithText(query));
        Log.d("Number of entries ", String.valueOf(mEntries.size()));
        mAdapter.notifyDataSetChanged();
    }
}
