package com.ntomita.comicmanager;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.PowerManager;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.extensions.android.json.AndroidJsonFactory;
import com.google.api.client.googleapis.services.AbstractGoogleClientRequest;
import com.google.api.client.googleapis.services.GoogleClientRequestInitializer;
import com.ntomita.backend.registration.Registration;
import com.ntomita.comicmanager.db.BookEntry;
import com.ntomita.comicmanager.db.CatalogDbHelper;
import com.ntomita.comicmanager.db.FavDbHelper;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.net.ssl.HttpsURLConnection;

public class DetailActivity extends AppCompatActivity {
    final String TAG = this.getClass().getSimpleName();
    private String mKey;
    private String mPath;
    private String mAuthor;
    private String mTitle;
    private String mURL;
    private Boolean mIsFolder;
    private String mParent;

    private ImageView mThumbnailView;
    private TextView mTitleView;
    private TextView mAuthorView;
    private ListView mChildrenView;

    private ImageButton mOpenButton;
    private ImageButton mFavButton;
    private ImageButton mDeleteButton;
    private ImageButton mPrevButton;
    private ImageButton mNextButton;
    private ImageButton mPlayButton;

    private Drawable mDownloadIcon;
    private Drawable mOpenIcon;
    private Drawable mFavIcon;
    private Drawable mUnfavIcon;
    private Drawable mDeleteDisableIcon;
    private Drawable mDeleteAbleIcon;
    private Drawable mPrevIcon;
    private Drawable mNextIcon;
    private Drawable mPlayIcon;


    private Context mContext;
    private ProgressDialog mProgressDialog;

    Intent readerIntent;
    BookEntry nextBook;
    AlertDialog.Builder builder;

    final String ICON_COLOR_GREEN = "#00E676";
    final String ICON_COLOR_RED = "#FF1744";

    //private CatalogDbHelper mDbHelper;
    private FavDbHelper favDbHelper;
    private boolean isFaved;

    private boolean startDownloadThisBook = false;
    private boolean completedDownloadThisBook = false;

    DownloadFileTask task;

    public static String SERVER_ADDR = "http://livemyruns.appspot.com/";

    /*
    *   LIFE CYCLE
    * */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        final LinearLayout summaryIconCardView = (LinearLayout) findViewById(R.id.summary_field_icons);

        if (savedInstanceState != null){
            startDownloadThisBook = savedInstanceState.getBoolean("startDownloadThisBook");
            completedDownloadThisBook = savedInstanceState.getBoolean("completedDownloadThisBook");
        }

        Intent i = getIntent();

        mContext = this;

        mKey = i.getStringExtra("KEY");
        mPath = i.getStringExtra("PATH");
        mAuthor = i.getStringExtra("AUTHOR");
        mTitle = i.getStringExtra("TITLE");
        mURL = i.getStringExtra("URL");
        mIsFolder = i.getBooleanExtra("ISFOLDER", true);
        mParent = i.getStringExtra("PARENT");

        mThumbnailView = (ImageView) findViewById(R.id.item_thumbnail_detail);
        mTitleView = (TextView) findViewById(R.id.item_title_detail);
        mAuthorView = (TextView) findViewById(R.id.item_author_detail);
        try {
            mThumbnailView.setImageDrawable(Drawable.createFromStream(getAssets().open("thumbnails/" + mKey + ".jpg"), null));
        }catch (Exception e){
        }

        mTitleView.setText(mTitle);
        mAuthorView.setText(mAuthor);

        initIcons();

        // ADD COMPONENTS

        if (!mIsFolder){
            // ADD BUTTON: DELETE BUTTON
            final File fileLocation = new File(Environment.getExternalStorageDirectory().getAbsolutePath()+ "/" + this.getApplicationInfo().loadLabel(this.getPackageManager()) + "/Books/"+mPath);
            mDeleteButton = new ImageButton(this);
            mDeleteButton.setBackgroundColor(Color.TRANSPARENT);
            mDeleteButton.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            mDeleteButton.setPadding(getDP(4), getDP(4), getDP(4), getDP(4));
            mDeleteButton.setImageDrawable(mDeleteDisableIcon);
            mDeleteButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                    builder.setTitle("Are you sure you want to delete a downloaded file?");
                    builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            fileLocation.delete();
                            Toast.makeText(mContext, "Deletion successful!", Toast.LENGTH_SHORT).show();
                            startDownloadThisBook = false;
                            completedDownloadThisBook = false;
                            mDeleteButton.setImageDrawable(mDeleteDisableIcon);
                            mDeleteButton.setEnabled(false);
                            mPlayButton.setImageDrawable(mDownloadIcon);
                            dialog.dismiss();
                        }
                    });
                    builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    AlertDialog dialog = builder.create();
                    dialog.show();

                }
            });
            mDeleteButton.setEnabled(false);
            summaryIconCardView.addView(mDeleteButton);
            if (fileLocation.exists()){
                mDeleteButton.setImageDrawable(mDeleteAbleIcon);
                mDeleteButton.setEnabled(true);
            }


            // ADD BUTTON: PREVIOUS BUTTON
            mPrevButton = new ImageButton(this);
            mPrevButton.setBackgroundColor(Color.TRANSPARENT);
            mPrevButton.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            mPrevButton.setPadding(getDP(4), getDP(4), getDP(4), getDP(4));
            mPrevButton.setImageDrawable(mPrevIcon);
            mPrevButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    BookEntry item = getDeltaVolume(mPath, -1);
                    if (item != null){
                        Intent i = new Intent(mContext, DetailActivity.class);
                        i.putExtra("KEY", item.getKey());
                        i.putExtra("PATH", item.getPath());
                        i.putExtra("AUTHOR", item.getAuthor());
                        i.putExtra("TITLE", item.getTitle());
                        i.putExtra("URL", item.getURL());
                        i.putExtra("ISFOLDER", item.getIsFolder());
                        i.putExtra("PARENT", item.getParent());
                        finish();
                        mContext.startActivity(i);
                    }else{
                        Toast.makeText(mContext, "No previous volume", Toast.LENGTH_SHORT).show();
                    }
                }
            });
            summaryIconCardView.addView(mPrevButton);


            // ADD BUTTON: PLAY BUTTON
            mPlayButton = new ImageButton(this);
            mPlayButton.setBackgroundColor(Color.TRANSPARENT);
            mPlayButton.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            mPlayButton.setPadding(getDP(4), getDP(4), getDP(4), getDP(4));
            if (fileLocation.exists()){
                mPlayButton.setImageDrawable(mPlayIcon);
            }else{
                mPlayButton.setImageDrawable(mDownloadIcon);
            }

            mPlayButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    File thisFile = new File(Environment.getExternalStorageDirectory().getAbsolutePath()+ "/" + mContext.getApplicationInfo().loadLabel(mContext.getPackageManager()) + "/Books/"+mPath);
                    // This book have downloaded
                    // But make sure that the file is not corrupted
                    if (thisFile.exists() && !(startDownloadThisBook && !completedDownloadThisBook)){

                        readerIntent = new Intent(mContext, ReaderActivity.class);
                        readerIntent.putExtra("FULLPATH", Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + mContext.getApplicationInfo().loadLabel(mContext.getPackageManager()) + "/Books/" + mPath);

                        nextBook = getDeltaVolume(mPath, 1);
                        if (nextBook != null && !(new File(Environment.getExternalStorageDirectory().getAbsolutePath()+ "/" + mContext.getApplicationInfo().loadLabel(mContext.getPackageManager()) + "/Books/"+nextBook.getPath()).exists())) { // Next Volume Exists
                            builder = new AlertDialog.Builder(mContext);
                            builder.setTitle("Do you want to download the next volume while reading?");
                            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    readerIntent.putExtra("NEXT_PATH", nextBook.getPath());
                                    readerIntent.putExtra("NEXT_URL", nextBook.getURL());
                                    dialog.dismiss();
                                    mContext.startActivity(readerIntent);
                                }
                            });
                            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    mContext.startActivity(readerIntent);
                                }
                            });
                            AlertDialog dialog = builder.create();
                            dialog.show();
                        }else{
                            mContext.startActivity(readerIntent);
                        }



                    }else{
                        // BOOK NOT EXISTS: DOWNLOAD
                        if (thisFile.exists()){
                            thisFile.delete();
                            startDownloadThisBook = false;
                            completedDownloadThisBook = false;
                        }
                        downloadBook(mContext, mPath, mURL);
                    }
                }
            });
            summaryIconCardView.addView(mPlayButton);


            // ADD BUTTON: NEXT BUTTON
            mNextButton = new ImageButton(this);
            mNextButton.setBackgroundColor(Color.TRANSPARENT);
            mNextButton.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            mNextButton.setPadding(getDP(4), getDP(4), getDP(4), getDP(4));
            mNextButton.setImageDrawable(mNextIcon);
            mNextButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    BookEntry item = getDeltaVolume(mPath, 1);
                    if (item != null) {
                        Intent i = new Intent(mContext, DetailActivity.class);
                        i.putExtra("KEY", item.getKey());
                        i.putExtra("PATH", item.getPath());
                        i.putExtra("AUTHOR", item.getAuthor());
                        i.putExtra("TITLE", item.getTitle());
                        i.putExtra("URL", item.getURL());
                        i.putExtra("ISFOLDER", item.getIsFolder());
                        i.putExtra("PARENT", item.getParent());
                        finish();
                        mContext.startActivity(i);
                    } else {
                        Toast.makeText(mContext, "No next volume", Toast.LENGTH_SHORT).show();
                    }
                }
            });
            summaryIconCardView.addView(mNextButton);

        }




        // ADD BUTTON: FAV BUTTON
        favDbHelper = new FavDbHelper(this);
        mFavButton = new ImageButton(this);
        mFavButton.setBackgroundColor(Color.TRANSPARENT);
        mFavButton.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        mFavButton.setPadding(getDP(4), getDP(4), getDP(4), getDP(4));
        isFaved = favDbHelper.isFaved(mKey);
        if (isFaved){
            mFavButton.setImageDrawable(mFavIcon);
        }else{
            mFavButton.setImageDrawable(mUnfavIcon);
        }
        mFavButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isFaved) {
                    // TURN TO UNFAVED
                    mFavButton.setImageDrawable(mUnfavIcon);
                    favDbHelper.deleteFav(mKey);
                    (new FavFragment()).updateFavView();


                    new AsyncTask<Void, Void, String>() {

                        @Override
                        // Get history and upload it to the server.
                        protected String doInBackground(Void... arg0) {



                            String uploadState = "";


                            // Upload the history of all entries using upload().

                            try {


                                Map<String, String> params = new HashMap<String, String>();
                                params.put("book_id", String.valueOf("hello"));
                                params.put("user_id", String.valueOf("dude"));

                                ServerUtilities.post(SERVER_ADDR + "/delete.do", params);
                            } catch (IOException e1) {
                                uploadState = "Sync failed: " + e1.getCause();
                                Log.e("TAGG", "data posting error " + e1);
                            }




                            return uploadState;
                        }

                        @Override
                        protected void onPostExecute(String errString) {
                            String resultString;
                            if(errString.equals("")) {
                                resultString =  " entry deleted.";
                            } else {
                                resultString = errString;
                            }

                            Toast.makeText(DetailActivity.this, resultString,
                                    Toast.LENGTH_SHORT).show();

                        }

                    }.execute();




                } else {
                    // TURN TO FAVED
                    mFavButton.setImageDrawable(mFavIcon);
                    favDbHelper.addFav(mKey);
                    System.out.println("favorite icon press");

//                    r book_id, user_id, user_name, and group_name.



                    new AsyncTask<Void, Void, String>() {

                        @Override
                        // Get history and upload it to the server.
                        protected String doInBackground(Void... arg0) {



                            String uploadState = "";


                                // Upload the history of all entries using upload().

                                try {


                                    Map<String, String> params = new HashMap<String, String>();
                                    params.put("book_id", String.valueOf("hello"));
                                    params.put("user_id", String.valueOf("dude"));
                                    params.put("user_name", String.valueOf("test"));
                                    params.put("group_name", "shit");

                                    ServerUtilities.post(SERVER_ADDR + "/add.do", params);
                                } catch (IOException e1) {
                                    uploadState = "Sync failed: " + e1.getCause();
                                    Log.e("TAGG", "data posting error " + e1);
                                }




                            return uploadState;
                        }

                        @Override
                        protected void onPostExecute(String errString) {
                            String resultString;
                            if(errString.equals("")) {
                                resultString =  " entry uploaded.";
                            } else {
                                resultString = errString;
                            }

                            Toast.makeText(DetailActivity.this, resultString,
                                    Toast.LENGTH_SHORT).show();

                        }

                    }.execute();






                }
                isFaved = !isFaved;
            }
        });
        summaryIconCardView.addView(mFavButton);

        if (mIsFolder == true){
            mChildrenView = (ListView) findViewById(R.id.item_children_list_view);
            CatalogDbHelper catalogDbHelper = new CatalogDbHelper(this);
            ArrayList<BookEntry> mDataset = catalogDbHelper.queryEntriesWithParent(mKey);
            Log.d("Number of data: ", String.valueOf(mDataset.size()));
            ListViewAdapter adapter = new ListViewAdapter(this, R.layout.list_item, mDataset);
            mChildrenView.setAdapter(adapter);

            // ADD BUTTON: OPEN IN BROWSER
            mOpenButton = new ImageButton(this);
            mOpenButton.setBackgroundColor(Color.TRANSPARENT);
            mOpenButton.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            mOpenButton.setPadding(getDP(4), getDP(4), getDP(4), getDP(4));
            mOpenButton.setImageDrawable(mOpenIcon);
            mOpenButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Uri uri = Uri.parse(mURL);
                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                    startActivity(intent);
                }
            });
            summaryIconCardView.addView(mOpenButton);
        }

        Log.d(TAG, "BEFORE ASYNC TASK");
        new GcmRegistrationAsyncTask(this).execute();


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (startDownloadThisBook && ! completedDownloadThisBook){
            File thisFile = new File(Environment.getExternalStorageDirectory().getAbsolutePath()+ "/" + mContext.getApplicationInfo().loadLabel(mContext.getPackageManager()) + "/Books/"+mPath);
            if (thisFile.exists()){
                thisFile.delete();
            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putBoolean("startDownloadThisBook", startDownloadThisBook);
        outState.putBoolean("completedDownloadThisBook", completedDownloadThisBook);
    }

    /*
    *   END: LIFE CYCLE
    * */




    /*
    *   Support Functions
    * */
    public BookEntry getDeltaVolume(String path, int delta){
        int substringLength = 8; // substring in which a index for volume may be included
        if (path.length() - substringLength < 0){
            return null;
        }
        String narrowedTarget = path.substring(path.length() - substringLength);
        Pattern pattern = Pattern.compile("\\d{2,3}");
        Matcher matcher = pattern.matcher(narrowedTarget);
        if (matcher.find()){
            String curVol = narrowedTarget.substring(matcher.start(), matcher.end());
            String nexVol = String.format("%0" + String.valueOf(matcher.end() - matcher.start()) + "d", Integer.parseInt(curVol) + delta);
            String nexFulPath = path.substring(0, path.length() - substringLength) + matcher.replaceFirst(nexVol);
            CatalogDbHelper dbHelper = new CatalogDbHelper(mContext);
            BookEntry entry = dbHelper.queryEntryMatchWith(nexFulPath);
            if (entry != null){
                return entry;
            }
        }

        return null;
    }

    public int getDP(int px){
        return (int)(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, px, getResources().getDisplayMetrics()));
    }

    public void initIcons(){
        mDownloadIcon = DrawableCompat.wrap(mContext.getResources().getDrawable(R.drawable.ic_file_download_36dp));
        DrawableCompat.setTint(mDownloadIcon.mutate(), Color.parseColor(ICON_COLOR_GREEN));
        mOpenIcon = DrawableCompat.wrap(mContext.getResources().getDrawable(R.drawable.ic_open_in_browser_36dp));
        DrawableCompat.setTint(mOpenIcon.mutate(), Color.parseColor(ICON_COLOR_GREEN));
        mFavIcon = DrawableCompat.wrap(mContext.getResources().getDrawable(R.drawable.ic_favorite_36dp));
        DrawableCompat.setTint(mFavIcon.mutate(), Color.parseColor(ICON_COLOR_RED));
        mUnfavIcon = DrawableCompat.wrap(mContext.getResources().getDrawable(R.drawable.ic_favorite_outline_36dp));
        DrawableCompat.setTint(mUnfavIcon.mutate(), Color.parseColor(ICON_COLOR_RED));
        mDeleteDisableIcon = DrawableCompat.wrap(mContext.getResources().getDrawable(R.drawable.ic_delete_36dp));
        DrawableCompat.setTint(mDeleteDisableIcon.mutate(), Color.LTGRAY);
        mDeleteAbleIcon = DrawableCompat.wrap(mContext.getResources().getDrawable(R.drawable.ic_delete_36dp));
        DrawableCompat.setTint(mDeleteAbleIcon.mutate(), Color.parseColor(ICON_COLOR_RED));
        mPrevIcon = DrawableCompat.wrap(mContext.getResources().getDrawable(R.drawable.ic_skip_previous_black_36px));
        DrawableCompat.setTint(mPrevIcon.mutate(), Color.parseColor(ICON_COLOR_RED));
        mNextIcon = DrawableCompat.wrap(mContext.getResources().getDrawable(R.drawable.ic_skip_next_36dp));
        DrawableCompat.setTint(mNextIcon.mutate(), Color.parseColor(ICON_COLOR_RED));
        mPlayIcon = DrawableCompat.wrap(mContext.getResources().getDrawable(R.drawable.ic_play_arrow_36dp));
        DrawableCompat.setTint(mPlayIcon.mutate(), Color.parseColor(ICON_COLOR_RED));

    }

    private void downloadBook(Context context, final String bookPath, String bookURL){
        String[] params = new String[2];
        params[0] = mURL.replaceFirst("redir", "download");
        Log.d("URL:", params[0]);
        params[1] = mContext.getApplicationInfo().loadLabel(mContext.getPackageManager()) + "/Books/" + mPath;

        mProgressDialog = new ProgressDialog(mContext);
        mProgressDialog.setMessage("Downloading a file");
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        mProgressDialog.setCancelable(true);

        startDownloadThisBook = true;
        task = new DownloadFileTask(mContext);
        task.execute(params);

        mProgressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {

                task.cancel(true);
                final File tempFile = new File(Environment.getExternalStorageDirectory().getAbsolutePath()+ "/" + mContext.getApplicationInfo().loadLabel(mContext.getPackageManager()) + "/Books/"+bookPath);
                if (tempFile.exists()){
                    tempFile.delete();
                }
            }
        });
    }


    /*
    *   Asynctasks
    * */
    public class DownloadFileTask extends AsyncTask<String, Integer, String>{
        private Context context;
        private PowerManager.WakeLock mWakeLock;

        public DownloadFileTask(Context context){
            this.context = context;
        }

        @Override
        protected String doInBackground(String... params) {
            InputStream inStream=null;
            OutputStream outStream=null;
            HttpsURLConnection connection=null;

            try{
                URL url = new URL(params[0]);
                connection = (HttpsURLConnection) url.openConnection();
                connection.setInstanceFollowRedirects(true);
                //connection.setDoInput(true);
                //connection.setDoOutput(true);
                connection.connect();

                if (connection.getResponseCode() != HttpURLConnection.HTTP_OK){
                    return "Server returned HTTP " + connection.getResponseCode() + " " + connection.getResponseMessage();
                }

                int fileLength = connection.getContentLength();

                inStream = connection.getInputStream();
                File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath(), params[1]);
                outStream = new FileOutputStream(file);

                byte data[] = new byte[4096];
                long total = 0;
                int count;
                while ((count = inStream.read(data)) != -1){
                    if (isCancelled()){
                        inStream.close();
                        return null;
                    }
                    total += count;
                    if (fileLength > 0){
                        publishProgress((int)(total * 100 / fileLength));
                    }
                    outStream.write(data, 0, count);
                }

            }catch (MalformedURLException e){
                e.printStackTrace();
            }catch (IOException e){
                e.printStackTrace();
            } finally {
                try{
                    if (outStream != null){
                        outStream.close();
                    }
                    if (inStream != null){
                        inStream.close();
                    }
                }catch (IOException e){
                    e.printStackTrace();
                }
                if (connection != null){
                    connection.disconnect();
                }
            }

            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            PowerManager pm = (PowerManager) context.getSystemService(context.POWER_SERVICE);
            mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, getClass().getName());
            mWakeLock.acquire();
            mProgressDialog.show();
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);

            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setMax(100);
            mProgressDialog.setProgress(values[0]);
        }

        @Override
        protected void onPostExecute(String s) {
            //super.onPostExecute(s);
            mWakeLock.release();
            mProgressDialog.dismiss();
            if (s != null){
                Toast.makeText(context, "Download failed: "+s, Toast.LENGTH_LONG).show();
            }else{
                Toast.makeText(context, "File downloaded", Toast.LENGTH_SHORT).show();
            }

            completedDownloadThisBook = true;
            mDeleteButton.setImageDrawable(mDeleteAbleIcon);
            mDeleteButton.setEnabled(true);
            mPlayButton.setImageDrawable(mPlayIcon);
        }
    }




    class GcmRegistrationAsyncTask extends AsyncTask<Void, Void, String> {
        private Registration regService = null;
        private GoogleCloudMessaging gcm;
        private Context context;

        // TODO: change to your own sender ID to Google Developers Console project number, as per instructions above
//        private static final String SENDER_ID = "816116540785";

        //use for project id for livemyruns
        private static final String SENDER_ID = "810724279392";

        public GcmRegistrationAsyncTask(Context context) {
            this.context = context;
        }

        @Override
        protected String doInBackground(Void... params) {
            if (regService == null) {
//                Registration.Builder builder = new Registration.Builder(AndroidHttp.newCompatibleTransport(),
//                        new AndroidJsonFactory(), null)
//                        // Need setRootUrl and setGoogleClientRequestInitializer only for local testing,
//                        // otherwise they can be skipped
//                        .setRootUrl(SERVER_ADDR +"/_ah/api/")
//                        .setGoogleClientRequestInitializer(new GoogleClientRequestInitializer() {
//                            @Override
//                            public void initialize(AbstractGoogleClientRequest<?> abstractGoogleClientRequest)
//                                    throws IOException {
//                                abstractGoogleClientRequest.setDisableGZipContent(true);
//                            }
//                        });

                //live myruns

                Registration.Builder builder = new Registration.Builder(AndroidHttp.newCompatibleTransport(), new AndroidJsonFactory(), null)
                        .setRootUrl("https://livemyruns.appspot.com/_ah/api/");


                // end of optional local run code

                regService = builder.build();
            }

            String msg = "";
            try {
                if (gcm == null) {
                    gcm = GoogleCloudMessaging.getInstance(context);
                }
                String regId = gcm.register(SENDER_ID);
                msg = "Device registered, registration ID=" + regId;

                // You should send the registration ID to your server over HTTP,
                // so it can use GCM/HTTP or CCS to send messages to your app.
                // The request to your server should be authenticated if your app
                // is using accounts.
                regService.register(regId).execute();

            } catch (IOException ex) {
                ex.printStackTrace();
                msg = "Error: " + ex.getMessage();
            }
            return msg;
        }

        @Override
        protected void onPostExecute(String msg) {
            Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
            Logger.getLogger("REGISTRATION").log(Level.INFO, msg);
        }
    }



}
