package com.ntomita.comicmanager;

import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ntomita.comicmanager.db.BookEntry;

import junit.framework.Assert;

import java.io.InputStream;
import java.util.ArrayList;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ItemViewHolder> {
    final String TAG = this.getClass().getSimpleName();
    private Context mContext;
    private ArrayList<BookEntry> mDataset;

    // ITEM VIEW HOLDER
    public class ItemViewHolder extends RecyclerView.ViewHolder{
        CardView mCardView;
        ImageView mThumbnail;
        TextView mAuthor;
        TextView mTitle;

        public ItemViewHolder(View itemView) {
            super(itemView);
        }

        public ItemViewHolder(View v, CardView cv, ImageView iv, TextView tv1, TextView tv2)

        {
            super(v);
            mCardView = cv;
            mThumbnail = iv;
            mTitle = tv1;
            mAuthor = tv2;
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d("ItemViewHolder", "onClick");
                    Intent i = new Intent(mContext, DetailActivity.class);
                    BookEntry item = mDataset.get(getAdapterPosition());
                    i.putExtra("KEY", item.getKey());
                    i.putExtra("PATH", item.getPath());
                    i.putExtra("AUTHOR", item.getAuthor());
                    i.putExtra("TITLE", item.getTitle());
                    i.putExtra("URL", item.getURL());
                    i.putExtra("ISFOLDER", item.getIsFolder());
                    i.putExtra("PARENT", item.getParent());
                    mContext.startActivity(i);
                }
            });

        }

        public ItemViewHolder newInstance(View parent){
            CardView cv = (CardView) parent.findViewById(R.id.cardView);
            ImageView iv = (ImageView) parent.findViewById(R.id.item_thumbnail);
            TextView tv1 = (TextView) parent.findViewById(R.id.item_title);
            TextView tv2 = (TextView) parent.findViewById(R.id.item_author);
            return new ItemViewHolder(parent, cv, iv, tv1, tv2);
        }

    }

    public RecyclerViewAdapter(Context mContext, ArrayList<BookEntry> mDataset) {
        this.mContext = mContext;
        this.mDataset = mDataset;
    }

    @Override
    public RecyclerViewAdapter.ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_item, parent, false);
        ItemViewHolder itemViewHolder = new ItemViewHolder(v);
        return itemViewHolder.newInstance(v);
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position) {
        BookEntry entry = mDataset.get(position);

        holder.mAuthor.setText(entry.getAuthor());
        holder.mTitle.setText(entry.getTitle());
        holder.mThumbnail.setImageDrawable(getDrawable(mContext, entry.getKey()));

    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public Drawable getDrawable(Context context, String name){
        Assert.assertNotNull(context);
        Assert.assertNotNull(name);

        AssetManager am = context.getAssets();
        try{
            InputStream ims = am.open("thumbnails/" + name + ".jpg");
            // load image as Drawable
            Drawable d = Drawable.createFromStream(ims, null);
            // set image to ImageView
            return d;
        }catch (Exception e){
            return null;
        }

    }

}
