package com.ntomita.comicmanager;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import java.io.File;

public class MainActivity extends AppCompatActivity {
    final String TAG = this.getClass().getSimpleName();
    private ViewPager mViewPager;
    TabLayout tabLayout;
    Context mContext;

    private final int PERMISSION_REQUEST_READ_EXT = 0;
    private final int PERMISSION_REQUEST_WRITE_EXT = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mContext = getApplicationContext();

        int permissionCheck_write = ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (permissionCheck_write != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_WRITE_EXT);
        }


        // CREATE FOLDERS TO STORE DB AND COMICS (FIX FOR GALAXY DEVICES)
        File sysDir = new File(Environment.getExternalStorageDirectory().getAbsolutePath()+ "/" + this.getApplicationInfo().loadLabel(this.getPackageManager()) + "/System/");
        sysDir.mkdirs();
        sysDir = new File(Environment.getExternalStorageDirectory().getAbsolutePath()+ "/" + this.getApplicationInfo().loadLabel(this.getPackageManager()) + "/Books/");
        sysDir.mkdirs();

        setupTablayout();


    }

    private void setupTablayout(){
        tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        tabLayout.addTab(tabLayout.newTab().setText("New"));
        tabLayout.addTab(tabLayout.newTab().setText("Books"));
        tabLayout.addTab(tabLayout.newTab().setText("Favorite"));

        final ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
        final PagerAdapter adapter = new FragmentPagerAdapter
                (getSupportFragmentManager()){
            @Override
            public Fragment getItem(int position) {
                Fragment fragment = null;
                if (position == 0){
                    // HOME
                    fragment = new HomeFragment();
                }else if (position == 1){
                    // ALL
                    fragment = new AllFragment();
                }else{
                    // FAVORITE
                    fragment = new FavFragment();
                }
                return fragment;
            }

            @Override
            public int getCount() {
                return 3;
            }
        };

        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        //super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode){
            case PERMISSION_REQUEST_WRITE_EXT:{
                if (grantResults.length >0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){

                }else{
                    finish();
                    System.exit(0);
                }
                return;
            }
        }
    }

    public void onSearchClicked(View view) {
        Intent intent = new Intent(AllFragment.mContext, SearchActivity.class);
        AllFragment.mContext.startActivity(intent);

    }
}
