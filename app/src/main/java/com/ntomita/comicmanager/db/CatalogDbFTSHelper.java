package com.ntomita.comicmanager.db;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by Naofumi on 2/23/16.
 */
public class CatalogDbFTSHelper extends SQLiteOpenHelper {
    Context mContext;
    public static String DATABASE_FILE_PATH;
    private static String DATABASE_FILE_NAME = "fts.db";
    private static int DATABASE_VERSION =1;

    private static ArrayList<BookEntry> allEntries;

    private static final String TABLE_NAME = "ITEMS";
    private static final String COLUMN_KEY = "KEY";
    private static final String COLUMN_PATH = "PATH";
    private static final String COLUMN_AUTHOR = "AUTHOR";
    private static final String COLUMN_TITLE = "TITLE";
    private static final String COLUMN_URL = "URL";
    private static final String COLUMN_DATE = "DATE";
    private static final String COLUMN_ISFOLDER = "ISFOLDER";
    private static final String COLUMN_PARENT = "PARENT";

    public CatalogDbFTSHelper(Context context){
        super(context, context.getExternalFilesDir(null).getAbsolutePath() + "/" + DATABASE_FILE_NAME, null, DATABASE_VERSION);
        mContext = context;
        DATABASE_FILE_PATH = mContext.getExternalFilesDir(null).getAbsolutePath();

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE VIRTUAL TABLE " + TABLE_NAME + " USING fts3 " +
                "(" + COLUMN_KEY + " TEXT, " +
                COLUMN_PATH + " TEXT, " +
                COLUMN_AUTHOR + " TEXT, " +
                COLUMN_TITLE + " TEXT, " +
                COLUMN_URL + " TEXT, " +
                COLUMN_DATE + " TEXT, " +
                COLUMN_ISFOLDER + " INTEGER, " +
                COLUMN_PARENT + " TEXT " +
                ")");
        CatalogDbHelper dbHelper = new CatalogDbHelper(mContext);
        allEntries = dbHelper.queryEntries();

        for (BookEntry entry: allEntries){
            db.execSQL("INSERT INTO "+TABLE_NAME+ " VALUES( \'"+ entry.getKey() + "\', " +
                            "\'"+ entry.getPath() + "\', " +
                            "\'"+ entry.getAuthor() + "\', " +
                            "\'"+ entry.getTitle() + "\', " +
                            "\'"+ entry.getURL() + "\', " +
                            "\'"+ entry.getDate() + "\', " +
                            "\'"+ entry.getIsFolder() + "\', " +
                            "\'"+ entry.getParent() + "\' " +
                    ") " );
        }

        dbHelper.close();
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public ArrayList<BookEntry> queryEntriesWithText(String text){
        Log.d("Query: ", text);
        SQLiteDatabase db = this.getReadableDatabase();
        String[] selectionArgs = {text};
        Cursor cursor = db.rawQuery("SELECT * FROM "+TABLE_NAME+" WHERE "+TABLE_NAME+ " MATCH ?", selectionArgs);
        //Cursor cursor = db.query(TABLE_NAME, null, COLUMN_AUTHOR + " LIKE \'" + text + "\' OR " + COLUMN_TITLE + " LIKE \'" + text + "\'", null, null, null, null);
        ArrayList<BookEntry> books = new ArrayList<BookEntry>();
        BookEntry tempBook;
        try{
            cursor.moveToFirst();
            while(! cursor.isAfterLast()){
                tempBook = new BookEntry();
                tempBook.setKey(cursor.getString(cursor.getColumnIndex(COLUMN_KEY)));
                tempBook.setPath(cursor.getString(cursor.getColumnIndex(COLUMN_PATH)));
                tempBook.setAuthor(cursor.getString(cursor.getColumnIndex(COLUMN_AUTHOR)));
                tempBook.setTitle(cursor.getString(cursor.getColumnIndex(COLUMN_TITLE)));
                tempBook.setURL(cursor.getString(cursor.getColumnIndex(COLUMN_URL)));
                tempBook.setIsFolder(cursor.getInt(cursor.getColumnIndex(COLUMN_ISFOLDER)) == 1 ? Boolean.TRUE : Boolean.FALSE);
                tempBook.setParent(cursor.getString(cursor.getColumnIndex(COLUMN_PARENT)));
                books.add(tempBook);
                cursor.moveToNext();
            }
        } finally {
            cursor.close();
        }
        db.close();
        Collections.sort(books);
        return books;
    }
}
