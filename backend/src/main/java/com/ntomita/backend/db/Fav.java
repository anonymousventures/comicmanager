package com.ntomita.backend.db;

/**
 * Created by Naofumi on 3/1/16.
 */
public class Fav {
    public static final String FAV_PARENT_ENTITY_NAME = "FavParent";
    public static final String FAV_PARENT_KEY_NAME = "FavParent";

    public static final String FAV_ENTITY_NAME = "Fav";
    public static final String FIELD_NAME_USER_ID = "UserId";
    public static final String FIELD_NAME_BOOK_ID = "BookId";

    public String mUserId;
    public String mBookId;

    public Fav(String _uid, String _bid){
        mUserId = _uid;
        mBookId = _bid;
    }

}
