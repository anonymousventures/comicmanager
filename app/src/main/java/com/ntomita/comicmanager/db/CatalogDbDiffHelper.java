package com.ntomita.comicmanager.db;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.ntomita.comicmanager.Globals;
import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by Naofumi on 2/20/16.
 */
public class CatalogDbDiffHelper extends SQLiteAssetHelper {
    private static final String DATABASE_NAME = "catalog_"+ Globals.DB_VERSION + "_diff.db";
    private static final int DATABASE_VERSION = 1;
    private Context mContext;
    private static ArrayList<BookEntry> entries;

    private static final String comic_root_id = Globals.COMIC_ROOT_ID;

    private static final String TABLE_NAME = "ITEMS";
    private static final String COLUMN_KEY = "KEY";
    private static final String COLUMN_PATH = "PATH";
    private static final String COLUMN_AUTHOR = "AUTHOR";
    private static final String COLUMN_TITLE = "TITLE";
    private static final String COLUMN_URL = "URL";
    private static final String COLUMN_DATE = "DATE";
    private static final String COLUMN_ISFOLDER = "ISFOLDER";
    private static final String COLUMN_PARENT = "PARENT";

    public CatalogDbDiffHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        mContext = context;
    }


    public ArrayList<BookEntry> queryEntries(){
        if (entries != null){
            return entries;
        }
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_NAME, null, null, null, null, null, null);
        ArrayList<BookEntry> books = new ArrayList<BookEntry>();
        BookEntry tempBook;
        try{
            cursor.moveToFirst();
            while(! cursor.isAfterLast()){
                tempBook = new BookEntry();
                tempBook.setKey(cursor.getString(cursor.getColumnIndex(COLUMN_KEY)));
                tempBook.setPath(cursor.getString(cursor.getColumnIndex(COLUMN_PATH)));
                tempBook.setAuthor(cursor.getString(cursor.getColumnIndex(COLUMN_AUTHOR)));
                tempBook.setTitle(cursor.getString(cursor.getColumnIndex(COLUMN_TITLE)));
                tempBook.setURL(cursor.getString(cursor.getColumnIndex(COLUMN_URL)));
                tempBook.setIsFolder(cursor.getInt(cursor.getColumnIndex(COLUMN_ISFOLDER)) == 1 ? Boolean.TRUE : Boolean.FALSE);
                tempBook.setParent(cursor.getString(cursor.getColumnIndex(COLUMN_PARENT)));
                books.add(tempBook);
                cursor.moveToNext();
            }
        } finally {
            cursor.close();
        }
        db.close();
        Collections.sort(books);
        entries = books;
        return books;
    }
}
