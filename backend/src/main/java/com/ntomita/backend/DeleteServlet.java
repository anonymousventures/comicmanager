package com.ntomita.backend;

import com.ntomita.backend.db.FavDatastore;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by Naofumi on 3/2/16.
 */
public class DeleteServlet extends HttpServlet{
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doGet(req, resp);
        String user_id = req.getParameter("user_id");
        String book_id = req.getParameter("book_id");

        boolean ret = FavDatastore.delete(user_id, book_id);

        if (ret){
            // delete success
        }{
            // delete failed
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //super.doPost(req, resp);
        doGet(req, resp);
    }
}
